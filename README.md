# Hide Folders

A quick toggle to hide the folders in the left sidebar.

This Toggle is theme agnostic.

If you always want to use the latest version (I sometimes tend to adjust a few little things here and there), you can use my deployed version here:

https://marcaux.gitlab.io/sn-hide-folders/ext.json

