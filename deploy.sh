#!/bin/sh
apt update
apt install zip
zip -r sn-hide-folders.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-hide-folders.zip public/
cd public
unzip sn-hide-folders.zip
